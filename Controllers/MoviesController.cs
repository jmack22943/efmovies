using AutoMapper;
using EFMovies.Dtos;
using EFMovies.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFMovies.Controllers;

[ApiController]
[Route("api/movies")]
public class MoviesController : ControllerBase
{
    private readonly ApplicationDbContext context;
    private readonly IMapper mapper;

    public MoviesController(ApplicationDbContext context, IMapper mapper)
    {
        this.context = context;
        this.mapper = mapper;
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<MovieDto>> Get(int id)
    {
        var movie = await context.Movies
            .Include(m => m.Genres)
            .Include(m => m.CinemaHalls)
                .ThenInclude(ch => ch.Cinema)
            .Include(m => m.MoviesActors)
                .ThenInclude(ma => ma.Actor)
            .FirstOrDefaultAsync(m => m.Id == id);

        if (movie is null)
        {
            return NotFound();
        }

        var movieDto = mapper.Map<MovieDto>(movie);
        movieDto.Cinemas = movieDto.Cinemas.DistinctBy(x => x.Id).ToList();
        
        return movieDto;
    }

    [HttpGet("selectloading/{id:int}")]
    public async Task<ActionResult> GetSelectLoading(int id)
    {
        var movieDto = await context.Movies.Select(m => new 
        {
            Id = m.Id,
            Title = m.Title,
            Genres = m.Genres.Select(g => g.Name).OrderByDescending(n => n)
        }).FirstOrDefaultAsync(m => m.Id == id);

        if (movieDto is null)
        {
            return NotFound();
        }

        return Ok(movieDto);
    }

}
