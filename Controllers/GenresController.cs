using AutoMapper;
using EFMovies.Dtos;
using EFMovies.Entities;
using EFMovies.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NetTopologySuite;
using NetTopologySuite.Geometries;

namespace EFMovies.Controllers;

[ApiController]
[Route("api/genres")]
public class GenresController : ControllerBase
{
    private readonly ApplicationDbContext context;
    private readonly IMapper mapper;

    public GenresController(ApplicationDbContext context, IMapper mapper)
    {
        this.context = context;
        this.mapper = mapper;
    }

    // [HttpGet]
    // public async Task<IEnumerable<Genre>> Get()
    [HttpGet]
    public async Task<IEnumerable<Genre>> Get(int page = 1, int recordsToTake = 2)
    {
        // if the Program.cs has the default set not to track then you can set the tracking like so:
        // return await context.Genres.AsTracking().ToListAsync();
        // return await context.Genres.ToListAsync();
        // If you want faster performance in your read only queries use this.
        // return await context.Genres.AsNoTracking().OrderBy(g => g.Name).ToListAsync();
        // // Order by name and return the first set of 2.
        // return await context.Genres.AsNoTracking().OrderBy(g => g.Name).Take(2).ToListAsync();
        // // Example of skipping over records for pagination.
        // return await context.Genres.AsNoTracking().OrderBy(g => g.Name).Skip(1).Take(2).ToListAsync();
        // Another example of pagination.
        // return await context.Genres.AsNoTracking()
        //     .OrderBy(g => g.Name)
        //     .Skip((page - 1) * recordsToTake)
        //     .Take(recordsToTake)
        //     .ToListAsync();
        return await context.Genres.AsNoTracking()
            .OrderBy(g => g.Name)
            .Skip((page - 1) * recordsToTake)
            .Paginate(page, recordsToTake)
            .ToListAsync();
        
    }

    [HttpGet("first")]
    public async Task<ActionResult<Genre>> GetFirst()
    {
        var genre = await context.Genres.FirstOrDefaultAsync(g => g.Name.Contains("z"));

        if (genre is null)
        {
            return NotFound();
        }

        return genre;
    }

    [HttpGet("filter")]
    public async Task<IEnumerable<Genre>> Filter(string name)
    {
        return await context.Genres.Where(g => g.Name.Contains(name)).ToListAsync();
    }

    // [HttpPost]
    // public async Task<ActionResult> Post(Genre genre)
    // {
    //     context.Add(genre); // marking genre as added.
    //     await context.SaveChangesAsync();
    //     return Ok();
    // }

    [HttpPost]
    public async Task<ActionResult> Post(GenreCreationDto genreCreationDto)
    {
        var genre = mapper.Map<Genre>(genreCreationDto);
        context.Add(genre); // marking genre as added.
        await context.SaveChangesAsync();
        return Ok();
    }
    
    // Add several records all at once.
    [HttpPost("several")]
    public async Task<ActionResult> Post(GenreCreationDto[] genresDto)
    {
        var genres = mapper.Map<Genre[]>(genresDto);
        context.AddRange(genres);
        await context.SaveChangesAsync();

        return Ok();
    }

    [HttpPost("cinemaWithRelated")]
    public async Task<ActionResult> Post()
    {
        var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);
        var cinemaLocation = geometryFactory.CreatePoint(new Coordinate(-69.913539, 18.476256));
        var cinema = new Cinema()
        {
            Name = "GKC Cinema - Elkhart",
            Location = cinemaLocation,
            CinemaOffer = new CinemaOffer()
            {
                DiscountPercentage = 5,
                Begin = DateTime.Today,
                End = DateTime.Today.AddDays(7)
            },
            CinemaHalls = new HashSet<CinemaHall>()
            {
                new CinemaHall()
                {
                    Cost = 200,
                    CinemaHallType = CinemaHallType.TwoDimensions,
                },
                new CinemaHall()
                {
                    Cost = 250,
                    CinemaHallType = CinemaHallType.ThreeDimensions,
                }
            }
        };
        
        context.Add(cinema);
        await context.SaveChangesAsync();

        return Ok();
    }

}
