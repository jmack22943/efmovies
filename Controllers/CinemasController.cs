using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFMovies.Dtos;
using EFMovies.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFMovies.Controllers;

[ApiController]
[Route("api/cinemas")]
public class CinemasController : ControllerBase
{
    private readonly ApplicationDbContext context;
    private readonly IMapper mapper;

    public CinemasController(ApplicationDbContext context, IMapper mapper)
    {
        this.context = context;
        this.mapper = mapper;
    }

    [HttpGet]
    public async Task<IEnumerable<CinemaDto>> Get()
    {
        // // This won't work out of the box. Must use ProjectTo the Cinema DTO
        // // Because of the type of properties on the Lat and Lon
        // return await context.Cinemas.ToListAsync();

        return await context.Cinemas
            .ProjectTo<CinemaDto>(mapper.ConfigurationProvider)
            .ToListAsync();
    }

    [HttpPost("withDto")]
    public async Task<ActionResult> Post(CinemaCreationDto cinemaCreationDto)
    {
        var cinema = mapper.Map<Cinema>(cinemaCreationDto);
        context.Add(cinema);
        await context.SaveChangesAsync();

        return Ok();
    }
}
