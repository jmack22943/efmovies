using AutoMapper;
using AutoMapper.QueryableExtensions;
using EFMovies.Dtos;
using EFMovies.Entities;
using EFMovies.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace EFMovies.Controllers;

[ApiController]
[Route("api/actors")]
public class ActorsController : ControllerBase
{
    private readonly ApplicationDbContext context;
    private readonly IMapper mapper;

    public ActorsController(ApplicationDbContext context, IMapper mapper)
    {
        this.context = context;
        this.mapper = mapper;
    }

    // [HttpGet]
    // public async Task<IEnumerable<Actor>> Get(int page = 1, int recordsToTake = 2)
    [HttpGet]
    public async Task<IEnumerable<ActorDto>> Get(int page = 1, int recordsToTake = 2)
    {
        // return await context.Actors.AsTracking()
        //     .OrderBy(g => g.Name)
        //     .Paginate(page, recordsToTake)
        //     .ToListAsync();
        // return await context.Actors.AsTracking()
        //     .OrderBy(g => g.Name)
        //     .Select(a => new ActorDto{Id = a.Id, Name = a.Name, DateOfBirth = a.DateOfBirth })
        //     .Paginate(page, recordsToTake)
        //     .ToListAsync();
        return await context.Actors.AsTracking()
            .OrderBy(g => g.Name)
            .ProjectTo<ActorDto>(mapper.ConfigurationProvider)
            .Paginate(page, recordsToTake)
            .ToListAsync();
    }

}
