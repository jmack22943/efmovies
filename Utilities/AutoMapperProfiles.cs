using AutoMapper;
using EFMovies.Dtos;
using EFMovies.Entities;
using NetTopologySuite;
using NetTopologySuite.Geometries;

namespace EFMovies.Utilities;

public class AutoMapperProfiles : Profile
{
    public AutoMapperProfiles()
    {
        CreateMap<Actor, ActorDto>();
        
        CreateMap<Cinema, CinemaDto>()
            .ForMember(
                dto => dto.Latitude, 
                ent => ent.MapFrom(p => p.Location.Y))
            .ForMember(
                dto => dto.Longitude,
                ent => ent.MapFrom(p => p.Location.X));

        CreateMap<Genre, GenreDto>();
        CreateMap<GenreCreationDto, Genre>();

        CreateMap<Movie, MovieDto>()
            .ForMember(
                dto => dto.Cinemas,
                ent => ent.MapFrom(p => p.CinemaHalls.Select(c => c.Cinema)))
            .ForMember(
                dto => dto.Actors, 
                ent => ent.MapFrom(p => p.MoviesActors.Select(ma => ma.Actor)));

        var geometryFactory = NtsGeometryServices.Instance.CreateGeometryFactory(srid: 4326);

        CreateMap<CinemaCreationDto, Cinema>()
            .ForMember(
                ent => ent.Location,
                dto => dto.MapFrom(
                    prop => geometryFactory.CreatePoint(
                        new Coordinate(prop.Longitude, prop.Latitude))));

        CreateMap<CinemaOfferCreationDto, CinemaOffer>();
        CreateMap<CinemaHallCreationDto, CinemaHall>();
    }
}     
 