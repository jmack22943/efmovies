using System.ComponentModel.DataAnnotations;

namespace EFMovies.Dtos;

public class GenreCreationDto
{
    [Required] 
    public string Name { get; set; }
}
