namespace EFMovies.Dtos;

public class CinemaCreationDto
{
    public string Name { get; set; }
    public double Latitude { get; set; }
    public double Longitude { get; set; }
    public CinemaOfferCreationDto CinemaOffer { get; set; }
    public CinemaHallCreationDto CinemaHalls { get; set; }
}
