namespace EFMovies.Dtos;

public class CinemaOfferCreationDto
{
    public double DiscountPercentage { get; set; }
    public DateTime Begin { get; set; }
    public DateTime End { get; set; }
}
