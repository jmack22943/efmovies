namespace EFMovies.Dtos;

public class ActorDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Biography { get; set; }
    public DateTime? DateOfBirth { get; set; }
}
