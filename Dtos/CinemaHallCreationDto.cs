using EFMovies.Entities;

namespace EFMovies.Dtos;

public class CinemaHallCreationDto
{
    public double Cost { get; set; }
    public CinemaHallType CinemaHallType { get; set; }
}
