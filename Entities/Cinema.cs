using NetTopologySuite.Geometries;

namespace EFMovies.Entities;

public class Cinema
{
    public int Id { get; set; }
    public string Name { get; set; }
    // public decimal Price { get; set; }
    public Point Location { get; set; }
    public CinemaOffer CinemaOffer { get; set; }
    // 3 examples of listing Cinema Halls
    // public List<CinemaHall> CinemaHalls { get; set; }
    public HashSet<CinemaHall> CinemaHalls { get; set; }
    // public ICollection<CinemaHall> CinemaHalls { get; set; }
}
